'use strict';

/**
 * Table class
 *
 * @param {Number} width
 * @param {Number} height
 * @constructor
 */
function Table(width, height) {
  this.width = width || 10;
  this.height = height || 10;

  return this;
}

Table.prototype = {
  width: null,
  height: null
};

module.exports = Table;
