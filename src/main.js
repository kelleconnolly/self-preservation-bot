'use strict';

var Table = require('./Table');
var Robot = require('./Robot');

var bot = new Robot(new Table(5, 5), handleOutput);
var inputElement = document.querySelector('[data-input]');
var outputElement = document.querySelector('[data-submit]');
var submitElement = document.querySelector('[data-submit]');

function handleOutput(output) {
  if (document.querySelector('[data-output]')) {
    document.querySelector('[data-output]').textContent = output;
  }
}

if ( inputElement && outputElement) {
  submitElement.addEventListener('click', function() {
    var instructions = inputElement.value.trim();
    bot.instruct(instructions);
  });
}
