'use strict';

var test = require('tape');
var Table = require('./Table');
var Robot = require('./Robot');

function outputStub() {}

function placeBotInstance(x, y, orientation) {
  var babybot = new Robot(new Table(5, 5), outputStub);
  return babybot.place(x, y, orientation);
}

test('Table initialisation', function(test) {
  test.plan(3);

  var contrstrainedTable = new Table(5, 5);
  var freeTable = new Table();

  // Check we have a table and that it's the right class.
  test.assert(contrstrainedTable instanceof Table, 'Table initialised and is Table class');

  // Check that the table is being created with the right dimensions
  test.assert(freeTable.width === 10 && freeTable.height === 10, 'Table is 10x10 if no dimensions are passed in');
  test.assert(contrstrainedTable.width === 5 && contrstrainedTable.height === 5, 'Table is 5x5 when we create our instance');
});

test('Robot initialisation', function(test) {
  test.plan(4);

  var testBot = placeBotInstance(1, 2, 'NORTH');

  // Check that we have a robot!
  test.assert(testBot instanceof Robot, 'Robot is initialised and is Robot class');

  // Check that out robot can output things
  var newBot = new Robot().report();
  test.assert(typeof newBot._handleOutput === 'function', 'Robot has an output function by default');

  // Check the Robot has a table associated with it
  test.assert(testBot.table instanceof Table, 'Table is added to the robot instance');
  // Check that if we don't pass a table in, it creates one all on its own
  test.assert(new Robot().table instanceof Table, 'Table is created if none is passed specified');
});

test('Robot placement', function(test) {
  test.plan(9);

  var testBot = placeBotInstance(1, 2, 'NORTH');

  // Check that our `place` function works as expected, and sets the state correctly
  test.equal(testBot.state.placed, true, 'Robot has a successfully `placed` state');
  test.assert(testBot.state.x === 1 && testBot.state.y === 2 && testBot.state.orientation === 'NORTH', 'Robot is placed at 1, 2, facing North');

  // Check that `place` fails if you try to place the robot outsitde of the table's bounds
  test.equal(placeBotInstance(6, 3, 'NORTH').state.placed, false, 'Robot cannot be placed on invalid x coordinates');
  test.equal(placeBotInstance(3, 6, 'NORTH').state.placed, false, 'Robot cannot be placed on invalid y coordinates');
  test.equal(placeBotInstance(-1, 3, 'NORTH').state.placed, false, 'Robot `place` function does not accept negative x values');
  test.equal(placeBotInstance(1, -3, 'NORTH').state.placed, false, 'Robot `place` function does not accept negative y values');

  // Check that `place` fails if you pass in an invalid orientation or coordinate
  test.equal(placeBotInstance(2, 2, 'Hogwarts').state.placed, false, 'Robot only accepts defined orientations');
  test.equal(placeBotInstance(2, 'Thirty five', 'NORTH').state.placed, false, 'Robot doesn\'t accept strings for coordinates');
  test.equal(placeBotInstance(2, 0.5, 'NORTH').state.placed, false, 'Robot only accepts whole numbers for coordinates');
});

test('Robot movement', function(test) {
  test.plan(8);

  var northBot = placeBotInstance(0, 2, 'NORTH').move();
  var southBot = placeBotInstance(0, 1, 'SOUTH').move();
  var westBot  = placeBotInstance(1, 0, 'WEST').move();
  var eastBot  = placeBotInstance(2, 0, 'EAST').move();

  // Check that our move function updates the state correctly
  test.equal(northBot.state.y, 3, 'Robot facing north moves up one unit');
  test.equal(southBot.state.y, 0, 'Robot facing south moves down one unit');
  test.equal(westBot.state.x, 0, 'Robot facing west moves left one unit');
  test.equal(eastBot.state.x, 3, 'Robot facing east moves right one unit');

  // Check our robot doesn't fall off the table
  northBot.place(2, 4, 'NORTH').move().move();
  test.equal(northBot.state.y, 4, 'Robot facing north doesn\'t keep moving when it hits a boundary');

  southBot.move();
  test.equal(southBot.state.y, 0, 'Robot facing south doesn\'t keep moving when it hits a boundary');

  westBot.move();
  test.equal(westBot.state.x, 0, 'Robot facing west doesn\'t keep moving when it hits a boundary');

  eastBot.place(4, 4, 'EAST').move().move().move();
  test.equal(eastBot.state.x, 4, 'Robot facing east doesn\'t keep moving when it hits a boundary');
});

test('Robot turning', function(test) {
  test.plan(9);

  // Check our left turns
  var testBot = placeBotInstance(0, 0, 'NORTH').left();
  test.equal(testBot.state.orientation, 'WEST', 'Robot starts facing north, turns left, ends up facing west');

  testBot.left();
  test.equal(testBot.state.orientation, 'SOUTH', 'Robot starts facing west, turns left, ends up facing south');

  testBot.left();
  test.equal(testBot.state.orientation, 'EAST', 'Robot starts facing south, turns left, ends up facing east');

  testBot.left();
  test.equal(testBot.state.orientation, 'NORTH', 'Robot starts facing east, turns left, ends up facing north');

  // Check our right turns
  testBot.right();
  test.equal(testBot.state.orientation, 'EAST', 'Robot starts facing north, turns right, ends up facing east');

  testBot.right();
  test.equal(testBot.state.orientation, 'SOUTH', 'Robot starts facing east, turns right, ends up facing south');

  testBot.right();
  test.equal(testBot.state.orientation, 'WEST', 'Robot starts facing south, turns right, ends up facing west');

  testBot.right();
  test.equal(testBot.state.orientation, 'NORTH', 'Robot starts facing west, turns right, ends up facing north');

  // Don't do anything if robot hasn't been placed
  var unplacedBot = new Robot(new Table(5, 5), outputStub);
  unplacedBot.left().right();
  test.assert(unplacedBot.state.placed === false && unplacedBot.state.orientation === null, 'No orientation set if robot is not placed');
});

test('Robot reporting', function(test){
  test.plan(4);

  // Check the output is what we expect
  var testBot = placeBotInstance(0, 0, 'NORTH').report();
  test.equal(testBot.reportedPosition, '0,0,NORTH', 'Placed robot reports position correctly');

  // Example inputs and outputs
  testBot.place(0, 0, 'NORTH').move().report();
  test.equal(testBot.reportedPosition, '0,1,NORTH', 'Placed and moved robot reports position correctly');

  testBot.place(0, 0, 'NORTH').left().report();
  test.equal(testBot.reportedPosition, '0,0,WEST', 'Placed and left turned robot reports position correctly');

  testBot.place(1, 2, 'EAST').move().move().left().move().report();
  test.equal(testBot.reportedPosition, '3,3,NORTH', 'Placed and left turned and moved robot reports position correctly');
});

test('Robot command input', function(test) {
  test.plan(6);

  // Check that we can pass in commands
  var testBot = new Robot(new Table(5, 5), outputStub).instruct('PLACE 0,0,NORTH').report();
  test.equal(testBot.reportedPosition, '0,0,NORTH', 'Robot placed with `instruct` method is positioned correctly');

  // Check that it fails if an invalid command is passed in
  var brokenBot = new Robot(new Table(5, 5), outputStub).instruct('DANCE');
  test.equal(brokenBot.state.placed, false, 'Command input only accepts defined commands');

  // Check that it fails if PLACE is called with the wrong number of arguments
  var stillBrokenBot = new Robot(new Table(5, 5), outputStub).instruct('PLACE 0,0');
  test.equal(stillBrokenBot.state.placed, false, 'Command PLACE fails if there isn\'t three arguments');

  // Example inputs and outputs
  testBot.instruct('PLACE 0,0,NORTH\nMOVE\nREPORT');
  test.equal(testBot.reportedPosition, '0,1,NORTH', 'Placed and moved robot reports position correctly');

  testBot.instruct('PLACE 0,0,NORTH\nLEFT\nREPORT');
  test.equal(testBot.reportedPosition, '0,0,WEST', 'Placed and left turned robot reports position correctly');

  testBot.instruct('PLACE 1,2,EAST\nMOVE\nMOVE\nLEFT\nMOVE\nREPORT');
  test.equal(testBot.reportedPosition, '3,3,NORTH', 'Placed and left turned and moved robot reports position correctly');

});
