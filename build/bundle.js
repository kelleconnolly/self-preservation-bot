(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var Table = require('./Table');

/**
 * List of valid orientations. Constant, so not on the class.
 *
 */
var ORIENTATIONS = [ 'NORTH', 'SOUTH', 'EAST', 'WEST' ];

/**
 * Robot class
 *
 * @param {Table} table
 * @param {function} handleOutput
 * @constructor
 */
function Robot(table, handleOutput) {
  this.table = table || new Table();
  this._handleOutput = handleOutput || this._defaultHandleOutput;
  // Setting state up here to avoid the thing where js does a shallow clone when creating a class.
  // We don't want to share state between our instances at all.
  this.state = {
    x: null,
    y: null,
    orientation: null,
    placed: false
  };
}

Robot.prototype = {
  table: null,

  /**
   * Place the robot at specified coordinates and set orientation.
   *
   * @returns {this}
   */
  place: function(x, y, orientation) {

    // Check that the x coordinate will fall within the table bounds
    if (!this._isValidCoordinates(x, y)) {
      this._handleMessageOutput('You can\'t place the bot here, your table is only ' + this.table.width + 'x' + this.table.height + ' units');
      return this;
    }

    if (!this._isValidOrientation(orientation)) {
      this._handleMessageOutput('That\'s not a valid orientation, use one of ' + ORIENTATIONS);
      return this;
    }

    this.state.x = x;
    this.state.y = y;
    this.state.orientation = orientation;
    this.state.placed = true;

    return this;
  },

  /**
   * Move the robot one unit in the direction it's facing.
   *
   * @returns {this}
   */
  move: function() {
    if (this.state.placed) {
      if (this.state.orientation === 'NORTH' && this.state.y + 1 < this.table.height) {
        this.state.y = this.state.y + 1;
      }
      if (this.state.orientation === 'SOUTH' && this.state.y - 1 >= 0) {
        this.state.y = this.state.y - 1;
      }
      if (this.state.orientation === 'WEST' && this.state.x - 1 >= 0) {
        this.state.x = this.state.x - 1;
      }
      if (this.state.orientation === 'EAST' && this.state.x + 1 < this.table.width) {
        this.state.x = this.state.x + 1;
      }
    }

    return this;
  },

  /**
   * Update the orientation of the robot by rotating left.
   *
   * @returns {this}
   */
  left: function() {
    if (!this.state.placed) {
      return this;
    }

    var orientationMap = {
      'NORTH': 'WEST',
      'WEST': 'SOUTH',
      'SOUTH': 'EAST',
      'EAST': 'NORTH'
    };

    this.state.orientation = orientationMap[this.state.orientation];
    return this;
  },

  /**
   * Update the orientation of the robot by rotating right.
   *
   * @returns {this}
   */
  right: function() {
    if (!this.state.placed) {
      return this;
    }

    var orientationMap = {
      'NORTH': 'EAST',
      'EAST': 'SOUTH',
      'SOUTH': 'WEST',
      'WEST': 'NORTH'
    };

    this.state.orientation = orientationMap[this.state.orientation];
    return this;
  },

  /**
   * Save and output a report of the current position and
   * orientation of the robot.
   *
   * @returns {this}
   */
  report: function() {
    var positionOutput = this._outputPosition();
    this._handleMessageOutput(positionOutput);
    return this;
  },

  /**
   * Accept, split, and parse commands to place, move, turn,
   * and report on the robot.
   *
   * @param {String} commands
   * @returns {this}
   */
  instruct: function(commands) {
    if (commands) {
      // Take the commands and split them into an array at each newline
      var splitCommands = commands.split('\n');

      splitCommands.forEach(function(command) {
        this._parseCommand(command);
      }.bind(this));
    }

    return this;
  },

  /**
   * Read and execute individual commands
   *
   * @param {String} command
   */
  _parseCommand: function(command) {
    var commandMap = {
      'PLACE': this.place,
      'MOVE': this.move,
      'LEFT': this.left,
      'RIGHT': this.right,
      'REPORT': this.report
    };
    // If the command has arguments, we just want the command for now
    var splitCommand = command.split(' ');
    var commandFunction = commandMap[ splitCommand[0] ];

    if (!commandFunction) {
      this._handleMessageOutput('Invalid command "' + command + '"');
      return;
    }

    // Handle PLACE specifically because it needs to have the right number of arguments
    if (splitCommand.length > 1 && splitCommand[0] === 'PLACE') {
      // Split out the arguments so we can check they're all there
      var commandArguments = splitCommand[1].split(',');
      if (commandArguments.length === 3) {
        // Coerce the coordinates into numbers so they pass validation
        var x = parseInt(commandArguments[0]);
        var y = parseInt(commandArguments[1]);
        var orientation = commandArguments[2];

        commandFunction.call(this, x, y, orientation);
      } else {
        this._handleMessageOutput('Invalid number of arguments for the PLACE command');
      }

    } else {
      // All other commands can just be called
      // But only call them if it's placed successfully, ie. don't override error messages.
      if (this.state.placed) {
        commandFunction.call(this);
      }
    }
  },

  /**
   * Save a string record of the current position and
   * orientation of the robot.
   *
   * @returns {String}
   */
  _outputPosition: function() {
    this.reportedPosition = this.state.x + ',' + this.state.y + ',' + this.state.orientation;
    return this.reportedPosition;
  },

  /**
   * Wrapper API for out message output function.
   *
   * @param {String} output
   */
  _handleMessageOutput: function(output) {
    this._handleOutput(output);
  },

  /**
   * Read and execute individual commands
   *
   * @param {String} command
   */
  _defaultHandleOutput: function(output) {
    console.log(output);
  },

  /**
   * Determine if value is a valid integer.
   *
   * @param {Number} n
   * @returns {Boolean}
   */
  _isValidInt: function(n) {
    return typeof n === 'number' && isFinite(n) && n % 1 === 0;
  },

  /**
   * Determine if coordinates fall within the bounds of the table.
   *
   * @param {Number} x
   * @param {Number} y
   */
  _isValidCoordinates: function(x, y) {
    var validity = true;

    // We want to check that these values are numbers, and that they're whole.
    if (!this._isValidInt(x) || !this._isValidInt(y)) {
      validity = false;
    }

    // Valid coordinates cannot exceed table bounds or be negative values.
    if (x > this.table.width || x < 0) {
      validity = false;
    }

    if (y > this.table.height || y < 0) {
      validity = false;
    }

    return validity;
  },

  /**
   * Determine if a value is among the options defined in
   * the ORIENTATIONS array.
   *
   * @param {String} orientation
   */
  _isValidOrientation: function(orientation) {
    return ORIENTATIONS.indexOf(orientation) !== -1;
  }

};

module.exports = Robot;

},{"./Table":2}],2:[function(require,module,exports){
'use strict';

/**
 * Table class
 *
 * @param {Number} width
 * @param {Number} height
 * @constructor
 */
function Table(width, height) {
  this.width = width || 10;
  this.height = height || 10;

  return this;
}

Table.prototype = {
  width: null,
  height: null
};

module.exports = Table;

},{}],3:[function(require,module,exports){
'use strict';

var Table = require('./Table');
var Robot = require('./Robot');

var bot = new Robot(new Table(5, 5), handleOutput);
var inputElement = document.querySelector('[data-input]');
var outputElement = document.querySelector('[data-submit]');
var submitElement = document.querySelector('[data-submit]');

function handleOutput(output) {
  if (document.querySelector('[data-output]')) {
    document.querySelector('[data-output]').textContent = output;
  }
}

if ( inputElement && outputElement) {
  submitElement.addEventListener('click', function() {
    var instructions = inputElement.value.trim();
    bot.instruct(instructions);
  });
}

},{"./Robot":1,"./Table":2}]},{},[3]);
