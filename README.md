#Self Preservation Bot

SPBot is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units by 5 units.

## Interface
Open `index.html` in your ES5 compatible browser of choice. Instructions for controlling the bot are on the page.

## Tests
Tests can be run in node by doing

    npm install

and then

    npm test

It's using, browserify, coverify, tape, and tap-spec under the hood.

### Latest test output

    Table initialisation
       - ✓ Table initialised and is Table class
       - ✓ Table is 10x10 if no dimensions are passed in
       - ✓ Table is 5x5 when we create our instance

    Robot initialisation
       - ✓ Robot is initialised and is Robot class
     null,null,null
       - ✓ Robot has an output function by default
       - ✓ Table is added to the robot instance
       - ✓ Table is created if none is passed specified

    Robot placement
       - ✓ Robot has a successfully `placed` state
       - ✓ Robot is placed at 1, 2, facing North
       - ✓ Robot cannot be placed on invalid x coordinates
       - ✓ Robot cannot be placed on invalid y coordinates
       - ✓ Robot `place` function does not accept negative x values
       - ✓ Robot `place` function does not accept negative y values
       - ✓ Robot only accepts defined orientations
       - ✓ Robot doesn't accept strings for coordinates
       - ✓ Robot only accepts whole numbers for coordinates

    Robot movement
       - ✓ Robot facing north moves up one unit
       - ✓ Robot facing south moves down one unit
       - ✓ Robot facing west moves left one unit
       - ✓ Robot facing east moves right one unit
       - ✓ Robot facing north doesn't keep moving when it hits a boundary
       - ✓ Robot facing south doesn't keep moving when it hits a boundary
       - ✓ Robot facing west doesn't keep moving when it hits a boundary
       - ✓ Robot facing east doesn't keep moving when it hits a boundary

    Robot turning
       - ✓ Robot starts facing north, turns left, ends up facing west
       - ✓ Robot starts facing west, turns left, ends up facing south
       - ✓ Robot starts facing south, turns left, ends up facing east
       - ✓ Robot starts facing east, turns left, ends up facing north
       - ✓ Robot starts facing north, turns right, ends up facing east
       - ✓ Robot starts facing east, turns right, ends up facing south
       - ✓ Robot starts facing south, turns right, ends up facing west
       - ✓ Robot starts facing west, turns right, ends up facing north
       - ✓ No orientation set if robot is not placed

    Robot reporting
       - ✓ Placed robot reports position correctly
       - ✓ Placed and moved robot reports position correctly
       - ✓ Placed and left turned robot reports position correctly
       - ✓ Placed and left turned and moved robot reports position correctly

    Robot command input
       - ✓ Robot placed with `instruct` method is positioned correctly
       - ✓ Command input only accepts defined commands
       - ✓ Command PLACE fails if there isn't three arguments
       - ✓ Placed and moved robot reports position correctly
       - ✓ Placed and left turned robot reports position correctly
       - ✓ Placed and left turned and moved robot reports position correctly

  tests 43
  pass  43



### coverage: 669/669 (100.00 %)

  Pass!
